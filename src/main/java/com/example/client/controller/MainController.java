package com.example.client.controller;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
	
	@GetMapping("/sampleTest")
	public String getData() {
		JSONObject returnObj=new JSONObject();
		returnObj.put("test2", "test3");
		return returnObj.toString();
	}

}
